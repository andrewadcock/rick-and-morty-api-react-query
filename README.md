## What
Simple react project to demonstrate React Query library usage.

## From
[https://www.youtube.com/watch?v=NQULKpW6hK4](https://www.youtube.com/watch?v=NQULKpW6hK4)
[https://github.com/harblaith7/React-Query/blob/main/src/App.css](https://github.com/harblaith7/React-Query/blob/main/src/App.css)