import React, {useEffect, useState} from 'react';
import { useQuery } from "react-query";
import Character from "./Character";

const Characters = () => {
    const [page, setPage] = useState(1);
    const [pages, setPages] = useState([]);

    const fetchCharacters = async ({queryKey}) => {
        const response = await fetch(`https://rickandmortyapi.com/api/character?page=${queryKey[1]}`);
        return response.json();
    }

    const {data, status} = useQuery(["characters", page], fetchCharacters, {
        keepPreviousData: true,
    })

    useEffect(() => {
        if(data && data.info) {
            setPages([...(Array(data.info.pages).keys())])
        }
    }, [data])

    if(status === "loading") {
        return <div>Loading...</div>
    }
    if( status === "error") {
        return <div>Error</div>
    }

    const handlePrevious = () => {
        setPage(page - 1)
    }

    const handleNext = () => {

        setPage(page + 1)

    }

    const handlePageSelect = (pageId) => {
        setPage(pageId)
    }

    return (
        <div className="characters">
            {data.results && data.results.map((character, i) =>
                <Character character={character} key={i} />
            )}
            <div>
                <button onClick={handlePrevious} disabled={ page <= 1}>Previous</button>
                {pages.map(currentPage => {

                    let style = (currentPage + 1 === page) ? {fontWeight: 'bold'} : {fontWeight: 'normal'}

                    return <a
                        key={currentPage}
                        onClick={(e) => handlePageSelect(currentPage + 1)}
                        // style={currentPage === page && 'fontWeight: bold'}
                        style={style}
                    >
                        {currentPage + 1}
                    </a>
                        })}
                <button onClick={handleNext} disabled={data && page === data.info.pages }>Next</button>
            </div>
        </div>
    );
};

export default Characters;